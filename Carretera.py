KM = int(input("Digite el total de KMs recorridos: "))
PrecioGas = int(input("Digite el precio de la gasolina x litro: "))
Pesos = int(input("Digite el dinero en gasolina gastado en el viaje: "))
Minutos = int(input("Digite cuanto tiempo ha tardado en el recorrido en minutos: "))

Horas = Minutos / 60
Seg = Minutos * 60
Metros = KM * 1000
Euros = Pesos / 3600
Lts = Euros / PrecioGas
LtsKm = Lts / KM
EuKm = Euros / KM
ConsLts = LtsKm * 100
ConsEuros = EuKm * 100
VelMedKmxh = KM / Horas
VelMedMxs = Metros / Seg

print("El consumo es", ConsLts, "Lts y", ConsEuros, "Euros en 100 KMs")
print("El consumo es", LtsKm, "Lts y", EuKm, "Euros en 1 KM")
print("La velocidad media en Km/h es de ", VelMedKmxh)
print("La velocidad media en M/s es de ", VelMedMxs)