class Contacto:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.name = kwargs.get('nombre')
        self.telephone = kwargs.get('telephone')
        self.extracell = kwargs.get('extracell')
        self.direccion = kwargs.get('direccion')
        self.whatsapp = kwargs.get('whatsapp')
        self.telegram = kwargs.get('telegram')

    def crear_contacto(self):
        if self.extracell == "":
            contact = {'ID': self.id, 'NOMBRE': self.name, 'TELEFONO': self.telephone,
                       'DIRECCIÓN': self.direccion, 'WHATSAPP': self.whatsapp, 'TELEGRAM': self.telegram}
        else:
            contact = {'ID': self.id, 'NOMBRE': self.name, 'TELEFONO': (self.telephone, self.extracell),
                       'DIRECCIÓN': self.direccion, 'WHATSAPP': self.whatsapp, 'TELEGRAM': self.telegram}
        return contact


class agenda():
    def __init__(self):
        self.contact_list = []  # aqui se declara la lista de contactos

    def listContact(self, contacto):
        self.contact_list.append(contacto)
        return self.contact_list


ag = agenda()

while True:

    id = input("Digite id: ")
    nombre = input("Digite nombre: ")
    telefono = input("Digite teléfono: ")
    extracell = input("Ingrese su segundo telefono: ")
    direccion = str(input("Ingrese la direccion: "))
    whatsapp = bool(input("whatsapp?: "))
    telegram = bool(input("telegram?: "))

    if whatsapp:
        whatsapp = 'Si'
    else:
        whatsapp = 'No'

    if telegram:
        telegram = 'Si'
    else:
        telegram = 'No'

    saveContact = Contacto(id=id, nombre=nombre, telephone=telefono, extracell=extracell, direccion=direccion,
                           whatsapp=whatsapp, telegram=telegram)
    nuevoContacto = saveContact.crear_contacto()
    agenda_llena = ag.listContact(nuevoContacto)  # Agregando el nuevo contacto a la lista

    while True:
        resp = (input("\nIngrese 's' si desea ingresar otro contacto, caso contario 'n': "))

        if resp == 's' or resp == 'n':
            break

    if resp == 'n':
        break

for i in range(len(agenda_llena)):
    print(agenda_llena[i])

