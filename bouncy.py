def NumBouncy(n):
  
  # Se convierte el entero a string y se recorre digito por digito
  Creciente, Decreciente, num = False, False, str(n)

  for i in range(len(num)-1):

    # Condicional para saber si los digitos van en aumento
    if num[i+1] > num[i]:
      Creciente = True

    # Condicional para saber si los digitos van decreciendo
    elif num[i+1] < num[i]: 
      Decreciente = True
      
    if Creciente and Decreciente:
      # Entonces es Bouncy
      return True

  # Si solo es creciente o decreciente
  return False


n = 100
Porcent = 0
bouncy = 0

while Porcent < 0.99:

  n += 1
  # Llamado a funcion
  if NumBouncy(n):
    bouncy += 1

  # Obtener nivel de porcentaje
  Porcent = bouncy / n

  if Porcent == 0.50 and n == 538:
    print("50% -->", n)

  if Porcent == 0.90 and n == 21780:
    print("90% -->", n)
 
# Imprimir numero bouncy obtenido al finalizar bucle
print(f'{int(Porcent*100)}{"% --> "}{n}')
