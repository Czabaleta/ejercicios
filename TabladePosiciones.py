from tabulate import tabulate

input("*****Tabla de Posiciones de la Liga*****")

CantEquipos = int(input("Digite la Cantidad de Equipos: "))
PJ = int(input("Digite la cantidad de Partidos Jugados: "))
Equipos = []

for i in range(CantEquipos):
    input("-------------------------------------------")
    while True:
        Nombre = input("Digite Nombre del Equipo: ")
        PG = int(input("Digite la cantidad de Partidos Ganados: "))
        PE = int(input("Digite la cantidad de Partidos Empatados: "))
        PP = int(input("Digite la cantidad de Partidos Perdidos: "))
        P = PG+PE+PP
        if P == PJ:
            GF = int(input("Digite la cantidad de Goles a Favor: "))
            GC = int(input("Digite la cantidad de Goles en Contra: "))
            DG = GF - GC
            Pts = PG * 3 + PE * 1 + PP * 0
            Equipos.append((Nombre, PJ, PG, PE, PP, GF, GC, DG, Pts))
            break
        print("\n")
        print(" ERROR AL DIGITAR LAS CANTIDADES, INGRESELAS NUEVAMENTE ")
        print("\n")

Equipos.sort(key=lambda desc: desc[8], reverse=True)
print("\n")
print(tabulate(Equipos, headers=['NOMBRE', 'PJ', 'PG', 'PE', 'PP', 'GF', 'GC', 'DG', 'PTS']))

