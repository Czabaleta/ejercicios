N = int(input("Capacidad del Disco Duro: "))
while True:
    R = str(input("Digite 'T' si está en Terabytes o 'G' si está en Gigabytes: "))
    Re = R.upper()
    if Re == 'T' or Re == 'G':
        break

if Re == 'G':
    Cantidad = 1024 * N / 700
    CDs = round(Cantidad)
    print("Se requieren", CDs, "CDs")
else:
    if Re == 'T':
        Cantidad = 1048576 * N / 700
        CDs = round(Cantidad)
        print("Se requieren", CDs, "CDs")
