from tabulate import tabulate

while True:
    n = int(input("Ingrese el tamaño de la matriz impar: "))
    if n % 2 != 0:
        break

matriz = []

for i in range(n):
    matriz.append([])
    for j in range(n):
        matriz[i].append([])

i, j = 0, n // 2
matriz[i][j] = 1


def cuadrado_magico(matriz, i, j, n):

    for k in range(2, (n*n)+1):

        filas = i - 1
        columnas = j + 1

        if filas >= 0 and columnas < n and matriz[filas][columnas] == []:
            matriz[filas][columnas] = k
            i, j = filas, columnas

        elif filas < 0 and columnas > n-1:
            filas, columnas = i + 1, j
            matriz[filas][columnas] = k
            i, j = filas, columnas

        elif filas < 0:
            filas = filas + n
            matriz[filas][columnas] = k
            i, j = filas, columnas
        elif columnas > n-1:
            columnas = columnas - n
            matriz[filas][columnas] = k
            i, j = filas, columnas

        elif matriz[filas][columnas] is not None:
            filas, columnas = i + 1, j
            matriz[filas][columnas] = k
            i, j = filas, columnas

    return matriz


print(tabulate(cuadrado_magico(matriz, i, j, n), tablefmt='fancy_grid'))
