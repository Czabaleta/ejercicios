f = int(input("Digite la posicion del caballo en la fila: "))
c = int(input("Digite la posicion del caballo en la columna: "))

ajedrez = []
# Creo una matriz de 8 x 8
for i in range(8):
    ajedrez.append([])
    for j in range(8):
        ajedrez[i].append([])


def movimientosvalidos(f0, c0, ajedrez):
    # Todas las opciones del caballo
    opciones = [(-2, -1), (-2, +1), (+2, -1), (+2, +1), (-1, -2), (-1, +2), (+1, -2), (+1, +2)]
    PosicionesValidas = []
    for (i, j) in opciones:
        # Calculo las nuevas posiciones del caballo
        opfilas = f0 + i
        opcolumnas = c0 + j
        # Condicional para escoger solo las coordenadas que esten dentro de la matriz
        # y validar que no hayan coordenadas repetidas
        if 0 <= opfilas < 8 and 0 <= opcolumnas < 8 and ajedrez[opfilas][opcolumnas] == []:
            PosicionesValidas.append([opfilas, opcolumnas])

    return PosicionesValidas


cont = 8
ajedrez[f][c] = True

while cont > 0:

    cont = 8
    Moves = movimientosvalidos(f, c, ajedrez)
    # print("primeros mov", Moves)
    for (i, j) in Moves:
        Moves = movimientosvalidos(i, j, ajedrez)
        # Todos los movimientos posibles de cada coordenada
        # Obtengo las coordenadas de la ruta con menos movimientos posibles

        if len(Moves) < cont:
            f, c = i, j
            cont = len(Moves)

    ajedrez[f][c] = True


for i in range(8):
    print(ajedrez[i])

