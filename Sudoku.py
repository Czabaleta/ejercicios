sudoku = [[8, 0, 0, 0, 0, 0, 0, 0, 0],
          [0, 0, 3, 6, 0, 0, 0, 0, 0],
          [0, 7, 0, 0, 9, 0, 2, 0, 0],
          [0, 5, 0, 0, 0, 7, 0, 0, 0],
          [0, 0, 0, 0, 4, 5, 7, 0, 0],
          [0, 0, 0, 1, 0, 0, 0, 3, 0],
          [0, 0, 1, 0, 0, 0, 0, 6, 8],
          [0, 0, 8, 5, 0, 0, 0, 1, 0],
          [0, 9, 0, 0, 0, 0, 4, 0, 0]]


def coordenadas_vacias(sudoku):

    for i in range(9):
        for j in range(9):
            if sudoku[i][j] == 0:
                return i, j

    return 10, 10


def validador(sudoku, i, j, num):

    fila, columna = False, False
    for k in range(9):
        # Se valida si el numero no esta ni en la fila ni en la columna
        if num != sudoku[i][k] and num != sudoku[k][j]:
            fila, columna = True, True
        else:
            fila, columna = False, False
            break

    if fila and columna:

        fila3x3 = 3 * (i // 3)
        col3x3 = 3 * (j // 3)  # Parte entera del cociente
        # Matriz 3x3
        for m in range(fila3x3, fila3x3 + 3):
            for n in range(col3x3, col3x3 + 3):
                if num == sudoku[m][n]:
                    return False
        return True

    return False


def resolver_sudoku(sudoku):

    i, j = coordenadas_vacias(sudoku)
    if i == 10:
        # Finaliza el programa
        return True

    for num in range(1, 10):
        # Se comprueba con la función validador si los valores son correcots
        # y se actualiza la matriz
        if validador(sudoku, i, j, num):
            sudoku[i][j] = num
            # Salta hacia la siguiente coordenada y valida el siguiente numero junto con su fila y columna
            if resolver_sudoku(sudoku):
                return True
            # Se deshace el valor de la celda actual si el valor no pasa la validacion
            sudoku[i][j] = 0
    # Si validador es false
    return False


resolver_sudoku(sudoku)
for i in range(9):
    print(sudoku[i])
