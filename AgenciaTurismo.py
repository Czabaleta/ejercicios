class Agencias:

    listaagencia = []

    def __init__(self, codigo, nombre, direccion):
        self.codigo = codigo
        self.nombre = nombre
        self.direccion = direccion
        self.conductores_list = []

    """def __str__(self) -> str:
        return f'Codigo: {self.codigo} ---- Nombre: {self.nombre} ---- Direccion: {self.direccion}'"""

    def listaConductores(self, conductores):
        self.conductores_list.append(conductores)
        agency = f'Codigo: {self.codigo} ---- Nombre: {self.nombre} ---- ' \
            f'Direccion: {self.direccion} ---- Conductor: {self.conductores_list}'
        return agency


class Persona(object):
    "Clase que representa una persona."
    def __init__(self, identificacion, name, lastname, genre, age, nacionalidad):
        "Constructor de Persona"
        self.identificacion = identificacion
        self.name = name
        self.lastname = lastname
        self.genre = genre
        self.age = age
        self.nacionalidad = nacionalidad

    def crear_persona(self):
        person = {'identificacion': self.identificacion, 'name': self.name, 'lastname': self.lastname,
                  'genre': self.genre, 'age': self.age, 'nacionalidad': self.nacionalidad}
        return person


class Conductores(Persona, Agencias):

    def __init__(self, identificacion, id, name, lastname, genre, age, nacionalidad, agenciaCod):
        Persona.__init__(self, identificacion, name, lastname, genre, age, nacionalidad)
        self.agenciaCod = agenciaCod
        self.id = id

    def crear_conductor(self):

        conductor = {'identificacion': self.identificacion, 'id': self.id, 'name': self.name, 'lastname': self.lastname,
                     'genre': self.genre, 'age': self.age, 'nacionalidad': self.nacionalidad}
        return conductor

    #  Esto lo puso el profe

    """def __str__(self) -> str:
        return f'Identificacion: {self.identificacion} ---- Nombre: {self.nombre}'"""


class Turistas:

    def __init__(self, **kwargs):
        self.identificacion = kwargs.get('identificacion')
        self.name = kwargs.get('name')
        self.lastname = kwargs.get('lastname')
        self.genre = kwargs.get('genre')
        self.age = kwargs.get('age')
        self.nacionalidad = kwargs.get('acionalidad')

    def crear_turista(self):
        turista = {'identificacion': self.identificacion, 'name': self.name, 'lastname': self.lastname,
                    'genre': self.genre, 'age': self.age, 'nacionalidad': self.nacionalidad}
        return turista

    def __str__(self) -> str:
        return f'Identificacion: {self.identificacion} ---- Nombre: {self.name}'


class Tours:

    def __init__(self, **kwargs):
        self.codigo = kwargs.get('codigo')
        self.destino = kwargs.get('destino')
        self.fecha = kwargs.get('fecha')
        self.costo = kwargs.get('costo')
        self.duracion = kwargs.get('duracion')

    def crear_tour(self, crear_conductor, crear_turista, **kwargs):
        tour = {'codigo': self.id, 'destino': self.destino, 'fecha': self.fecha, 'costo': self.duracion}
        return tour


saveAgencia = Agencias(codigo=10, nombre='DataTours', direccion='Manga cra 8')
saveCond = Conductores(identificacion=45456789, id=1, name='oscar', lastname='mendez',
                              genre='M', age=34, nacionalidad='Colombia', agenciaCod=10).crear_conductor()
saveCond1 = Conductores(identificacion=45776789, id=2, name='miguel', lastname='diaz',
                               genre='M', age=34, nacionalidad='Colombia', agenciaCod=10).crear_conductor()
saveCond2 = Conductores(identificacion=33336789, id=3, name='andres', lastname='george',
                               genre='M', age=34, nacionalidad='Colombia', agenciaCod=10).crear_conductor()

saveCond.get(Conductores)

Agencia = saveAgencia.listaConductores(saveCond)
Agencia = saveAgencia.listaConductores(saveCond1)
Agencia = saveAgencia.listaConductores(saveCond2)


print(Agencia)
