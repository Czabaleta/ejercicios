cadena = input("Ingresa palabra o frase: ")
C = cadena.replace(" ", "")
CadInv = C[::-1]
Cont = 0
L = len(C)
Porc = 0
for i in range(L):
    if CadInv[i] == C[i]:
        Cont = Cont + 1
        Prom = Cont / L
        Porc = Prom * 100

if Porc == 100:
    print("Es Palindromo (", Porc, "% )")
else:
    if Porc >= 80:
        print("Es casi Palindromo en un (", Porc, "% )")
    else:
        print("No es Palindromo (", Porc, "% )")