N = int(input("Digite el No. de estudiantes: "))
Alumnos = []
Notas = []
ListaProm = []
Media = 0

for i in range(N):
    print("\n")
    Nombre = input("Digite nombre del estudiante: ")
    Alumnos.append(Nombre)

    for j in range(6):
        Cant = float(input("Digite calificaciones del estudiante: "))
        Notas.append(Cant)
        Media = Media + Cant / 6

    ListaProm.append(Media)
    Media = 0

Desc = [e for e in zip(Alumnos, ListaProm)]
Desc.sort(key=lambda desc: desc[1], reverse=True)
print("\nPromedios: ")
print("\n".join([str(e) for e in Desc]))